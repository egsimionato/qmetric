package qb9;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class DartSpringApplication {

    public static void main(String[] args) {
        System.out.println(" > Hello Dart Spring App !!");

        try (AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(DartSpringApplication.class)) {

        }
    }
}

