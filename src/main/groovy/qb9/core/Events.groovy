package qb9.core.events;

import java.util.EventObject;
import java.util.EventListener;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ApplicationEvent;

public class DomainEvent extends ApplicationEvent {

}

@FunctionalInterface
public interface DomainListener<E extends DomainEvent> extends ApplicationListener {
     //public void onApplicationEvent(E event);
}

public interface DomainEventPublisher {
    public void publish(DomainEvent event);
}

@org.springframework.context.event.EventListener
public @interface HookListener {
}
