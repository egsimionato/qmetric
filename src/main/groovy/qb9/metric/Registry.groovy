package qb9.metric;

import java.time.Instant;

public interface IMetricRegistry {

    public void syncMetricsOfDay();
    public void syncMetricsOfDay(Instant day);

}

public class MetricRegistry implements IMetricRegistry {

    public Meter mauMeter;
    public Reporter mauReporter;

    public MetricRegistry(Meter mauMeter, Reporter mauReporter) {
        this.mauMeter = mauMeter;
        this.mauReporter = mauReporter;
    }

    public void syncMetricsOfDay() {
        syncMetricsOfDay(Instant.now());
    }

    public void syncMetricsOfDay(Instant day) {
        Metered mauOfDay = mauMeter.gauge(day);
        mauReporter.report(mauOfDay);
    }

    public void setMauMeter(Meter meter) {
        this.mauMeter = meter;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        public Meter mauMeter;
        public Reporter mauReporter;


        public Builder onMau() {
            this.mauMeter = new RandomMeter(Metric.MAU);
            this.mauReporter = new ConsoleReporter(Metric.MAU);
            return this;
        }

        public MetricRegistry build() {
            return new MetricRegistry(
                mauMeter,
                mauReporter
            );
        }
    }
}


