package qb9.metric;

import java.time.Instant;

enum Metric {
    MAU
}

public class Metered {

    public Instant day;
    public long count;
    public Metric metric;

    public Metered(metric, day, count) {
        this.metric = metric;
        this.day = day;
        this.count = count;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Instant day;
        private long count;
        private Metric metric;

        public Builder metric(Metric metric) {
            this.metric = metric;
            return this;
        }

        public Builder day(Instant day) {
            this.day = day;
            return this;
        }

        public Builder count(long count) {
            this.count = count;
            return this;
        }

        public Metered build() {
            return new Metered(metric, day, count);
        }
    }

    @Override
    public String toString() {
        return "Metered [metric=" + metric + ", day=" + day + ", count=" + count + "]";
    }
}

public class MeteredMadeEvent {

}
