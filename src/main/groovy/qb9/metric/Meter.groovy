package qb9.metric;

//import qb9.core.events;
import qb9.commons.Math;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.context.ApplicationEvent;

import java.time.Instant;
import java.util.Random;

public interface Meter {

    public Metered gauge(Instant day);

}

public class MeteredEvent extends ApplicationEvent {

    private static final long serialVersionUID = 889202626288526113L;

    private Metered metered;

    public MeteredEvent(Object source, Metered metered) {
        super(source);
        this.metered = metered;
    }

}

@Service
public class RandomMeter implements Meter {

    @Autowired
    private ApplicationEventPublisher publisher;

    
    private static final long MAX = 3500l;
    private static final long MIN = 100l;

    private Metric metric;

    public RandomMeter(Metric metric) {
        this.metric = metric;
    }

    public  Metered gauge(Instant day) {
            Metered m = Metered.builder()
            .metric(metric)
            .day(day)
            .count(Math.randLong(MAX as long, MIN as long))
            .build();

            publisher.publishEvent(new MeteredEvent(this, m));
            return m;
    }
}

