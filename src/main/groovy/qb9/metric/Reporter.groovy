package qb9.metric;

import java.time.Instant;

interface Reporter {

    public void report(Metered metered);

}

public class ConsoleReporter implements Reporter {

    private Metric metric;

    public ConsoleReporter(Metric metric) {
       this.metric = metric;
    }

    public void report(Metered metered) {
        System.out.println("\n\n ##############################################################################");
        System.out.println(" #                              Mau Reporter                                  # ");
        System.out.println(" ############################################################################## ");
        System.out.println(" #                                                                            # ");
        System.out.println(" #  " + metered );
        System.out.println(" #                                                                            # ");
        System.out.println(" ##################################[ END ]#####################################\n\n ");
    }
}


