package qb9.dart;

import qb9.metric.*;

import org.springframework.context.ApplicationEventPublisher;

import spock.lang.*;

class DailyGaugeAndPullSpec extends spock.lang.Specification {

    def "Gauge the Daily Active Users and Pull to Google Sheets." () {
        given:"A metric registry builded with mau enabled"
        ApplicationEventPublisher publisher = Stub(ApplicationEventPublisher);
        Meter meter = Stub(Meter);

        MetricRegistry metrics = MetricRegistry.builder().onMau().build();
        metrics.setMauMeter(meter);

        when:"A mau is metered and reported to google sheets"
        metrics.syncMetricsOfDay();

        then:""
        true;
    }

}
